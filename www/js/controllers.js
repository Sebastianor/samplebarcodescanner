angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {
  var initFunc = (typeof initMWBS === 'function') ? initMWBS : function(mwbs, constants, dvc) {

    try {
        var mwregister = {
            'Android': {
                'MWB_CODE_MASK_25': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_39': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_93': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_128': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_AZTEC': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DM': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_PDF': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_QR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_RSS': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_CODABAR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_11': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_MSI': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DOTCODE': { 'username': '', 'key': '' }
            },
            'iOS': {
                'MWB_CODE_MASK_25': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_39': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_93': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_128': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_AZTEC': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DM': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_PDF': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_QR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_RSS': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_CODABAR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_11': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_MSI': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DOTCODE': { 'username': '', 'key': '' }
            },
            'Win32NT': {
                'MWB_CODE_MASK_25': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_39': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_93': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_128': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_AZTEC': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DM': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_PDF': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_QR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_RSS': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_CODABAR': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_11': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_MSI': { 'username': '', 'key': '' },
                'MWB_CODE_MASK_DOTCODE': { 'username': '', 'key': '' }
            }
        }
    } catch (e) {
        DebugService.log('ERROR OUTPUT');
        DebugService.log(e);
    }

    /* END registration settings */
    var platform = mwregister[dvc.platform];

    Object.keys(platform).forEach(function(reg_codes) {
        mwbs['MWBregisterCode'](constants[reg_codes], platform[reg_codes]['username'], platform[reg_codes]['key']);
    });


    // DebugService.log('JS registration ends: '+ (new Date()).getTime());
    // DebugService.log('JS Settings starts: '+ (new Date()).getTime());
    //settings portion, disable those that are not needed

    /* BEGIN settings CALLS */
    //if your code doesn't work after changing a few parameters, and there is no error output, uncomment the try-catch, the error will be output in your console
    //    try{
    //  mwbs['MWBsetInterfaceOrientation'] (constants.OrientationPortrait);
    //  mwbs['MWBsetOverlayMode'](constants.OverlayModeImage);
    mwbs['MWBenableHiRes'](true);
    //  mwbs['MWBenableFlash'](true);
    //  mwbs['MWBsetActiveCodes'](constants.MWB_CODE_MASK_128 | constants.MWB_CODE_MASK_39);
    //  mwbs['MWBsetLevel'](2);
    //  mwbs['MWBsetFlags'](constants.MWB_CODE_MASK_39, constants.MWB_CFG_CODE39_EXTENDED_MODE);
    //  mwbs['MWBsetDirection'](constants.MWB_SCANDIRECTION_VERTICAL | constants.MWB_SCANDIRECTION_HORIZONTAL);
    //  mwbs['MWBsetScanningRect'](constants.MWB_CODE_MASK_39, 20,20,60,60);
    //  mwbs['MWBenableZoom'](true);
    //  mwbs['MWBsetZoomLevels'](200, 400, 0);
    //  mwbs['MWBsetMinLength'](constants.MWB_CODE_MASK_39, 4);
    mwbs['MWBsetMaxThreads'](1);
    mwbs['MWBcloseScannerOnDecode'](true);
    //  mwbs['MWBuse60fps'](true);
    //  mwbs['MWBsetParam'](constants.MWB_CODE_MASK_DM, constants.MWB_PAR_ID_RESULT_PREFIX, constants.MWB_PAR_VALUE_RESULT_PREFIX_ALWAYS);
    //  mwbs['MWBduplicateCodeDelay'](1000);



    // DebugService.log('JS Settings ends: '+ (new Date()).getTime());
    //    }
    //    catch(e){
    //        DebugService.log(e);
    //    }

    /* END settings CALLS */

    /* CUSTOM JAVASCRIPT CALLS */
}
var callFunc = (typeof callbackMWBS === 'function') ? callbackMWBS : function(result) {


  /**
   * result.code - string representation of barcode result
   * result.type - type of barcode detected or 'Cancel' if scanning is canceled
   * result.bytes - bytes array of raw barcode result
   * result.isGS1 - (boolean) barcode is GS1 compliant
   * result.location - contains rectangle points p1,p2,p3,p4 with the corresponding x,y
   * result.imageWidth - Width of the scanned image
   * result.imageHeight - Height of the scanned image
   */
  if (result.type == 'Cancel') {
      //Perform some action on scanning canceled if needed
  } else if (result && result.code) {

      
  }
}
  $scope.start = function()
    {
      scanner.startScanning(initFunc, callFunc);
    }
})
.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
